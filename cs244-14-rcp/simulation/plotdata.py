#!/usr/bin/env python

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
import math

cwd = "/home/ubuntu/cs244-14-rcp/simulation"

class FlowData:

  def __init__(self, filename, color, marker, label=''):
    """ Initialize FlowData
    Give it the filename where the data resides and it will read in the
    flow sizes, the average completion times, and max completion times.

      color: The color matplotlib will use for the line.
      marker: The marker matplotlib will use for the line.
      label: The label for the line.
    """
    self.color = color
    self.marker = marker
    self.linestyle = ':'
    self.label = label
    self.flowsizes = []
    self.avg_ct = []
    self.max_ct = []

    f = open(filename, 'r')

    for line in f:
      datum = line.rsplit(' ')
      self.flowsizes.append(float(datum[0]))
      self.avg_ct.append(float(datum[6]))
      self.max_ct.append(float(datum[8]))

    f.close()

# Following are equal to those set in simulation and needed for
# slow start analysis.
RTT = 0.1                   # Sec
C = 2.4*1000000000/(1000*8) # Pkts/Sec
LOAD = 0.9

def slowstart(L):
  """ Calculates SlowStart
  Why Flow-Completion Time is the Right metric for Congestion Control
  and why this means we need new algorithms
  Nandita Dukkipati, Nick McKeown
  http://yuba.stanford.edu/techreports/TR05-HPNG-112102.pdf
  Page 2
  """
  return (math.log(L + 1, 2) + 0.5)*RTT + L/C

class SlowStart:

  def __init__(self, maxsize, color):
    """ Initialize SlowStart Data
    Calculates slowstart average completion time from 0 to maxsize flows.
    """
    self.color = color
    self.marker = None
    self.linestyle = '-'
    self.label = "Slow Start"

    self.flowsizes = list(xrange(maxsize + 1))
    self.avg_ct = map(slowstart, self.flowsizes)
    self.max_ct = self.avg_ct

def ps(L):
  """ Calculates Processor Sharing
  Processor Sharing Flows in the Internet
  Nandita Dukkipati, Masayoshi Kobayashi, Rui Zhang-Shen, and Nick McKeown
  http://yuba.stanford.edu/~nanditad/RCP-IWQoS.pdf
  Page 10 (Page 276 on article)
  """
  return 1.5*RTT + L/(C*(1-LOAD))

class ProcessorSharing:

  def __init__(self, maxsize, color):
    """ Initialize Processor Sharing Data
    Calculates given processor sharing algorithm.
    """
    self.color = color
    self.marker = None
    self.linestyle = ':'
    self.label = "Processor Sharing"

    self.flowsizes = list(xrange(maxsize + 1))
    self.avg_ct = map(ps, self.flowsizes)
    self.max_ct = self.avg_ct
    pass
  pass

def get_FCT_graphs(name, lines):
  # Average Flow Completion Time
  save_figure(lines,
              True,
              [0, 2000, 0.1, 100],
              "Average Flow Completion Time [sec]",
              "flow size [pkts] (normal scale)",
              "graphs/fig12-afct-rcp-" + name + ".png",
              True,
              False)

  # Average Flow Completion Time (Long flows)
  save_figure(lines,
              True,
              [1000, 100000, 0.1, 100],
              "Average Flow Completion Time [sec]",
              "flow size [pkts] (log scale)",
              "graphs/fig12-afct-long-rcp-" + name + ".png",
              True,
              True)

  # Max Flow Completion Time
  save_figure(lines,
              False,
              [0, 2000, 0.1, 100],
              "Max. Flow Completion Time [sec]",
              "flow size [pkts] (normal scale)",
              "graphs/fig12-maxct-rcp-" + name + ".png",
              True,
              False)
  pass

    
# Plotting
def save_figure(lines, is_avg, axis_range, ylabel, xlabel, savefn, log_y=True, log_x=False):
  fig = plt.figure()
  graph = fig.add_subplot(111)

  for line in lines:
    if is_avg:
      y_values = line.avg_ct
    else:
      y_values = line.max_ct
    graph.plot(line.flowsizes, y_values, linestyle=line.linestyle,
               color=line.color, marker=line.marker, label=line.label)

  if log_y:
    graph.set_yscale("log")
  if log_x:
    graph.set_xscale("log")

  for axis in [graph.xaxis, graph.yaxis]:
    axis.set_major_formatter(ScalarFormatter())
  graph.axis(axis_range)

  graph.set_ylabel(ylabel)
  graph.set_xlabel(xlabel)
  plt.legend(loc='upper left')
  plt.savefig(savefn, format="png")

# Start the graphing /////////////////////////////////////////////////////////

SHAPES = ['1.2', '2.2']

# Note: Graphs for our Custom Processor Sharing simulations are only for
# 1.2 as this is the more interesting case to look at.


rcpVersions =  ["orig","stale-5"]
lc = {"orig": 'm', "stale-1": 'c', "stale-5": 'g'}
pt = {"orig": "+", "stale-1": "x", "stale-5": "o"}

NAMES = [("arrival=fixed-cap=2.4-rtt=0.1-init_nr_flows=25", rcpVersions),\
         ("arrival=fixed-cap=2.4-rtt=0.1-init_nr_flows=5", rcpVersions)]

flowPath = cwd + "/lib/rcp/pareto-flowSizes/%s/flowSizeVsDelay-%s"
queuePath = cwd + "/lib/rcp/pareto-flowSizes/%s/queue-%s.tr"
rcpPath = cwd + "/lib/rcp/pareto-flowSizes/%s/rcp-%s.tr"

f = open("rates.plt", "w")

for name, versions in NAMES:
  rcp_f = {}
  rcp_l = {}
  rcp_r = {}

  lines = []
  gnuplots = []
  for version in versions:
    # FCT graphs
    logs = "%s-logs" % version
    label = "%s-RCP"%version
    rcp_f[version] = flowPath % (logs, name)
    rcp_l[version] = FlowData(rcp_f[version], lc[version],\
                              '+', label)
    # Rate graph
    lines.append(rcp_l[version])
    rcp_r[version] = rcpPath % (logs, name)
    gnuplots.append("\"%s\" using ($3):($15*8/1000000.0) with points pt \'%s\' title \"%s (rate in MBps)\""%(rcp_r[version], pt[version], label))
    pass
    
  get_FCT_graphs(name, lines)

  f.write("plot " + " ,\\\n".join(plot for plot in gnuplots) + "\n")
  f.write("set term png\n")
  f.write("set output \"graphs/rates-%s.png\"\n"%name)
  f.write("replot\n\n")    
  pass

f.close()

    
