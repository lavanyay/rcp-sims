#!/usr/bin/perl -s


if ($sim_end == "") {
    $sim_end = 100;
}
if ($cap == "") {
    $cap = 2.4;
}
if ($rtt == "") {
    $rtt = 0.1;
}
if ($meanFlowSize == "") {
    $meanFlowSize = 5000;
}
if ($init_nr_flows == "") {
    $init_nr_flows = 5;
}
if ($alpha == "") {
    $alpha = 0.1;
}
if ($beta == "") {
    $beta = 1;
}
if ($load == "") {
    $load = 0.9;
}
 
@pareto_shape = (1.2);



if ($arrival == 0) {
    $name = "arrival=fixed-cap=$cap-rtt=$rtt-init_nr_flows=$init_nr_flows";
} else {
    $name = "arrival=poisson-cap=$cap-rtt=$rtt-init_nr_flows=$init_nr_flows";
}

if ($arrival == 0) {
    print "for fixed: -sim_end=$sim_end -cap=$cap -rtt=$rtt -load=$load -numbneck=$numbneck -alpha=$alpha -beta=$beta -init_nr_flows=$init_nr_flows -meanFlowSize=$meanFlowSize -arrival=fixed\n";
  print "nice ns sim-rcp-pareto.tcl $sim_end $cap $rtt $load $numbneck $alpha $beta $init_nr_flows $meanFlowSize fixed \n";
    `nice ns sim-rcp-pareto.tcl $sim_end $cap $rtt $load $numbneck $alpha $beta $init_nr_flows $meanFlowSize fixed`
} else {
    print "for poisson: -sim_end=$sim_end -cap=$cap -rtt=$rtt -load=$load -numbneck=$numbneck -alpha=$alpha -beta=$beta -init_nr_flows=$init_nr_flows -meanFlowSize=$meanFlowSize -arrival=poisson (shape @pareto_shape[$i])\n";
    print "nice ns sim-rcp-pareto.tcl $sim_end $cap $rtt $load $numbneck $alpha $beta $init_nr_flows $meanFlowSize poisson @pareto_shape[$i] \n";
    `nice ns sim-rcp-pareto.tcl $sim_end $cap $rtt $load $numbneck $alpha $beta $init_nr_flows $meanFlowSize poisson @pareto_shape[$i]`;
}

# Move extra traces to logs folder
`mv flow.tr logs/flow-$name.tr`;
`mv queue.tr logs/queue-$name.tr`;
`mv rcp_status.tr logs/rcp-$name.tr`;



