#!/usr/bin/perl -s

if ($sim_end == "") {
    $sim_end = 100;
}
if ($cap == "") {
    $cap = 2.4;
}
if ($rtt == "") {
    $rtt = 0.1;
}
if ($meanFlowSize == "") {
    $meanFlowSize = 5000;
}
if ($init_nr_flows == "") {
    $init_nr_flows = 5;
}
if ($alpha == "") {
    $alpha = 0.1;
}
if ($beta == "") {
    $beta = 1;
}
if ($load == "") {
    $load = 0.9;
}

$C = $cap*1000000000/8;

if ($arrival == 0) {
    $name = "arrival=fixed-cap=$cap-rtt=$rtt-init_nr_flows=$init_nr_flows"
} else {
    $name = "arrival=poisson-cap=$cap-rtt=$rtt-init_nr_flows=$init_nr_flows"
}

@fNameIn = ("logs/queue-$name.tr");
@fNameOut = ("logs/linkUtil-$name");

for ($i=0; $i<=$#fNameIn; $i++) {

    open(fileOut, ">$fNameOut[$i]") or dienice("Can't open $fNameOut[$i] for writing: $!");
    open(fileIn, "$fNameIn[$i]") or dienice ("Can't open $fNameIn[$i]: $!");

    $prev = 0;
    $BDeparture = 0;
    while (<fileIn>) {
      chomp;
      @items = split;

      $totBDeparture = $items[9];
      $BDeparture = $totBDeparture - $prev;
      $prev = $totBDeparture;
      printf fileOut "$items[0] BDeparture_ $BDeparture Bqueue_ $items[3]\n";
    }
    print $totBDeparture/($items[0] * $C) ;
    print "\n";

    close(fileIn);
    close(fileOut);
}

